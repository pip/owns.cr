# El YAML de knot.conf no es estándar, así que para leerlo y volverlo a
# escribir hay que tomar algunas medidas.
require "yaml"

# Redefinir Array#to_yaml para que solo use el estilo con guiones para
# objetos.  Las Strings se devuelven como [string, string2]
class Array
  def to_yaml(yaml : YAML::Nodes::Builder)
    style = case self[0].raw
            when String
              YAML::SequenceStyle::FLOW
            else
              YAML::SequenceStyle::ANY
            end

    yaml.sequence(reference: self, style: style) do
      each &.to_yaml(yaml)
    end
  end
end

# Por ahora solo leemos el archivo y lo volvemos a volcar, para
# comprobar que estamos generando un archivo compatible.
yaml = File.open("/etc/knot/knot.conf") do |file|
  YAML.parse(file)
end

# Reemplazar las String con comillas simples por comillas dobles.  No
# vale la pena redefinir String#to_yaml porque no hay un
# YAML::ScalarStyle que las deshabilite del todo y Knot no interpreta
# comillas simples.
#
# También elimina el encabezado de YAML.
puts yaml.to_yaml.tr("'", "\"").sub(/\A---\n/, "")
