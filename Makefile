all: own

# Build only when source changes
owns: src/owns.cr
	crystal build --release $<
	strip --strip-all $@
